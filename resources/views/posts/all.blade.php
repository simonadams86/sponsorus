@extends('app')

@section('heading')
    Inbox
@stop

@section('content')

    @if(isset($data['posts']))

        @if(count($data['posts']))
            Latest Posts Submitted

            <ul class="list-group">
                @foreach($data['posts'] as $post)
                    <li class="list-group-item">
                        <a href="{{action('PostsController@showCommentsByPost', $post->post_id)}}">
                            {!! $post->title !!}
                        </a>
                        {{--TODO read_at for comments--}}
                        <span class="badge">{!! $post->count !!}</span>
                    </li>
                @endforeach
            </ul>

        @endif
    @endif
    @if(isset($data['comments']))

        @if(count($data['comments']))
            Latest Comments Made

            <ul class="list-group">
                @foreach($data['comments'] as $comment)
                    <li class="list-group-item">
                        <a href="{{action('CommentsController@showCommentsByUser', [$comment->post_id, $comment->other_id])}}">
                            {!! $comment->comment !!}
                        </a>
                        {{--TODO read_at for comments--}}
                        <span class="badge">{!! $comment->count !!}</span>
                    </li>
                @endforeach
            </ul>

        @endif
    @endif

    <a href="{{action('PostsController@create')}}"><i class="glyphicon glyphicon-share"></i>Create post</a>

@endsection
