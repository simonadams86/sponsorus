@extends('app')

@section('heading')
    Edit a post
@stop

@section('content')
    <div class="panel panel-heading">
        Welcome <strong>{!! Auth::user()->name !!}</strong>. Edit your post
    </div>

    {!! Form::model($data['post'],['method' => 'PATCH', 'action' => ['PostsController@update', $data['post']->post_id]]) !!}

    @include('forms.post')

    {!! Form::submit('Edit', ['class' => 'btn btn-success form-control']) !!}

    {!! Form::close() !!}
    <hr/>
    <a class="btn btn-toolbar" href="{{action('PostsController@showCommentsByPost', $data['post']->post_id)}}">Back</a>
@stop