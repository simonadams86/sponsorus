@extends('app')

@section('heading')
    Write a post
@stop

@section('content')
    <div class="panel panel-heading">
        Welcome <strong>{!! Auth::user()->name !!}</strong>. Write a post
    </div>

    {!! Form::open(['method' => 'POST', 'action' => ['PostsController@create']]) !!}

    @include('forms.post')

    {!! Form::submit('Post', ['class' => 'btn btn-success form-control']) !!}

    {!! Form::close() !!}
    <hr/>
    <a class="btn btn-toolbar" href="{{action('PostsController@index')}}">Back</a>
@stop