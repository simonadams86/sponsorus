@extends('app')

@section('heading')
    Post
@stop

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            {!! $data['post']->title !!}
        </div>
        <div class="panel-body">{{$data['post']->message}}</div>
        <div class="panel-body">
            <a class="btn btn-info" href="{{action('PostsController@edit', [$data['post']->post_id])}}">Edit</a>
        </div>
    </div>

    @if(count($data['comments']))
        Replies from:
        <ul class="list-group">
            @foreach($data['comments'] as $comment)
                <li class="list-group-item">
                    <a href="{{action('CommentsController@showCommentsByUser', [$data['post']->post_id, $comment->other_id])}}">
                        {!! $comment->name !!}
                    </a>
                    {{--TODO read_at for comments--}}
                    <span class="badge">{!! $comment->count !!}</span>
                </li>
            @endforeach
        </ul>
    @endif
    <a class="btn btn-toolbar" href="{{action('PostsController@index')}}">Back</a>
@endsection