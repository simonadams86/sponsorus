@extends('app')

@section('heading')
    Profile of:
@endsection

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            {{$data['user']->name}}
        </div>

        <div class="panel panel-body">
            {{$data['user']->description}}

        </div>
    </div>
    @if(count($data['posts']))

        @include('partials.posts', ['title' => 'Posts'])
    @endif
@endsection