@extends('app')

@section('heading')
    Marketplace
@stop

@section('content')

    <div class="panel panel-default">

        <div class="panel-heading">
            {{$data['sectionOneH']}}
        </div>

        <div class="panel panel-body">

            <table id="datatable-users" class="table table-hover table-striped table-bordered">
                <thead>
                <tr>
                    @foreach($data['users']['headers'] as $th)

                        <th>{{$th}}</th>

                    @endforeach
                </tr>
                </thead>
                <tfoot class="hidden">
                <tr>
                    @foreach($data['users']['headers'] as $th)

                        <th>{{$th}}</th>

                    @endforeach
                </tr>
                </tfoot>
                <tbody>


                {{--Filled by ajax request, see datatable.js--}}

                </tbody>
            </table>
        </div>
    </div>


    <div class="panel panel-default">

        <div class="panel-heading">
            Posts
        </div>

        <div class="panel panel-body">

            <table id="datatable-posts" class="table table-hover table-striped table-bordered">
                <thead>
                <tr>
                    @foreach($data['posts']['headers'] as $th)

                        <th>{{$th}}</th>

                    @endforeach
                </tr>
                </thead>
                <tfoot class="hidden">
                <tr>
                    @foreach($data['posts']['headers'] as $th)

                        <th>{{$th}}</th>

                    @endforeach
                </tr>
                </tfoot>
                <tbody>

                {{--Filled by ajax request, see datatable.js--}}

                </tbody>
            </table>
        </div>
    </div>
    @include('partials.modal-large')
@endsection