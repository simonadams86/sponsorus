@extends('app')

@section('heading')
    Dashboard
@stop

@section('content')

    @if(isset($data['users']))

        @if(count($data['users']))
            <div class="panel panel-default">
                <div class="panel-heading">
                    Last {{ count($data['users']) }} {{ Auth::user()->user_type ? 'companies' : 'clubs' }} to sign up
                </div>

                <ul class="list-group">
                    @foreach($data['users'] as $user)
                        <li class="list-group-item">
                            <div class="panel panel-heading">
                                <a href="{{action('MarketplaceController@profile', $user->user_id)}}">
                                    {{ $user->name }}
                                </a>
                            </div>
                            <div class="panel panel-body">{{$user->description}}</div>
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif
    @endif

    @if(isset($data['posts']))

        @if(count($data['posts']))

            @include('partials.posts', ['title' => 'Last '.count($data['posts']).' posts created'])

        @else
            {{ Auth::user()->user_type ? 'No one is looking to sponsor a club' : 'No clubs are looking for sponsors' }}
        @endif
    @endif

@endsection