<div class="modal modal-primary fade scale" id="modal-lg-primary" tabindex="-1" role="dialog"
     aria-labelledby="modal-lg-primary" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form id="form-content" class="form-horizontal" role="form" type="POST" action="#">
                {{ csrf_field() }}

                <input type="hidden" name="post_id" value="" id="post_id">
                <input type="hidden" name="user_id" value="" id="user_id">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    {{--Insert title here--}}
                    <h4 class="modal-title" id="modal-lg-primary-label">

                    </h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-raised btn-secondary btn-flat" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" name="submit" value="Submit" id="modal-save" class="btn btn-raised btn-primary btn-flat"
                            data-dismiss="modal">
                        Open
                    </button>
                </div>


            </form>

        </div>
    </div>
</div>