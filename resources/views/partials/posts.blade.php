<div class="panel panel-default">
    <div class="panel-heading">
        {{$title}}
    </div>

    <ul class="list-group">
        @foreach($data['posts'] as $post)
            <li class="list-group-item">
                <a href="{{action('CommentsController@showCommentsByUser', [$post->post_id, $post->user_id])}}">
                    {{ $post->title}}
                </a>

                @if($post->name)
                <div class="panel panel-header">
                    Post by <strong>{{$post->name}}</strong>
                </div>
                @endif

                <div class="panel panel-body">{{$post->message}}</div>
                <div class="row">
                    <div class="col-sm-3 col-centered text-center">
                        <a href="{{ action('CommentsController@showCommentsByUser', [$post->post_id, $post->user_id]) }}"
                           class="btn btn-info btn-block">Write</a>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
</div>