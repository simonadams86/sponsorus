<footer>
    <div class="container">
        <div class="row">

            <div class="col-sm-11 col-centered">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="footer-text">{{ config('app.name', 'SponsorUs') }} {{date('Y')}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>