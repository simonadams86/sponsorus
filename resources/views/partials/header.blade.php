<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="row">
            <div class="col-sm-11 col-centered">
                <div class="navbar-header">
                    <a href="/{!! Auth::guest() ? '' : 'dashboard' !!}"><img src="{{asset('img/logo.png')}}" style="width: auto; height: 51px;"></a>
                    {{--<a href="/{!! Auth::guest() ? '' : 'dashboard' !!}" class="navbar-brand">
                        {{ config('app.name', 'SponsorUs') }}
                    </a>--}}
                    {{--Static--}}
                    <button type="button" data-toggle="collapse" data-target="#main" class="navbar-toggle collapsed">
                        <span class="sr-only"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse navbar-right" id="main">
                    <ul class="nav navbar-nav">
                        @if (!Auth::guest())
                            <li class="nav-item">
                                <a class="nav-link" href="{{action('DashboardController@index')}}">Dashboard</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{action('MarketplaceController@index')}}">Marketplace</a>
                            </li>
                            <li class="dropdown nav-item">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false">
                                    <i class="glyphicon glyphicon-envelope hidden-xs"></i>
                                    <div class="visible-xs">
                                        Messages
                                        <span class="pull-right">
                                    <span class="caret"></span>
                                </span>
                                    </div>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a class="nav-link" href="{{action('PostsController@index')}}">Inbox</a>
                                    </li>
                                    <li>
                                        <a class="nav-link" href="{{action('PostsController@create')}}">Create post</a>
                                    </li>
                                </ul>

                                </a>
                            </li>
                            <li class="dropdown nav-item">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false">
                                    {{ Auth::user()->name }}
                                    <span class="pull-right">
                                <span class="caret"></span>
                            </span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ action('UpdateUserController@show') }}">Update Info</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                           onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();"
                                        >Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                              style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                        @if (Auth::guest())
                            <li><a href="{{action('WelcomeController@index')}}">Home</a></li>
                            <li><a href="{{action('WelcomeController@contact')}}">Contact</a></li>
                            <li><a href="{{action('WelcomeController@about')}}">About Us</a></li>
                            <li><a href="{{url('/login')}}">Login</a></li>
                            <li><a href="{{url('/register')}}">Register</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
