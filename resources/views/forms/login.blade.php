@extends('app')

@section('heading')
    Login
@stop

@section('content')

    {{--For more info https://laravelcollective.com/docs/5.2/html#checkboxes-and-radio-buttons--}}

    {{--Retarded right now, but whatever--}}
    <div class="row">
        <div class="col-sm-6 col-centered">
            {{--Login tab--}}
            {!! Form::open(['url' => '/login']) !!}
            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::label('email', 'Email:') !!}
                {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'example@dbu.dk']) !!}
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                {!! Form::label('password', 'Password:') !!}
                {!! Form::password('password', ['class' => 'form-control']) !!}
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                {!! Form::submit('Login', ['class' => 'btn btn-success form-control']) !!}
            </div>
            <a href="{{ url('/password/reset') }}">
                Forgot Your Password?
            </a>
            {!! Form::close() !!}
        </div>

    </div>
@stop
