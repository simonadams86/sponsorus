@extends('app')

@section('heading')
    Update your info
@stop

@section('content')
    {{--Sign up tab--}}
    <div class="row">
        <div class="col-sm-6 col-centered">
            {!! Form::model(Auth::user(), ['action' => 'UpdateUserController@update']) !!}
            {{ csrf_field() }}

            @include('forms.user-details', ['submitBtnText' => 'Update'])

            @include('errors.form-errors')

            {!! Form::close() !!}
        </div>
    </div>
@stop