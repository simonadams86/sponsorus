@extends('app')

@section('heading')
    Register
@stop

@section('content')
    {{--Sign up tab--}}
    <div class="row">
        <div class="col-sm-6 col-centered">
            {!! Form::open(['url' => '/register']) !!}
            {{ csrf_field() }}

            <div class="form-group">
                {!! Form::label('user_type', 'Select:') !!}
                {!! Form::select('user_type', ['Company', 'Club'], null, ['class' => 'form-control']) !!}
                {{--{!! Form::select('user_type', []) !!}--}}
            </div>

            <div class="form-group">
                {!! Form::label('name', 'Name:') !!}
                {{--name, default, attribute array--}}
                {!! Form::text('name', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('password', 'Password:') !!}
                {{--name, default, attribute array--}}
                {!! Form::password('password', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('password_confirmation', 'Re-enter:') !!}
                {{--name, default, attribute array--}}
                {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('email', 'Email:') !!}
                {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'example@dbu.dk']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('phone', 'Phone:') !!}
                {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => '+45 12 34 56 78']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('description', 'Description:') !!}
                {{--name, default, attribute array--}}
                {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Sign up', ['class' => 'btn btn-success form-control']) !!}
            </div>

            @include('errors.form-errors')

            {!! Form::close() !!}
        </div>
    </div>
@stop

