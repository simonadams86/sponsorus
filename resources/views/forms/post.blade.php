{!! Form::label('title', 'Title:') !!}
{!! Form::text('title', null, ['class' => 'form-control']) !!}
{!! Form::label('message', 'Write your needs and demands:') !!}
{!! Form::textarea('message', null, ['class' => 'form-control']) !!}
@include('errors.form-errors')