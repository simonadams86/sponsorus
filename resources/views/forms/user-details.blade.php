<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    {{--name, default, attribute array--}}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('password', 'Update password:') !!}
    {{--name, default, attribute array--}}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('password_confirmation', 'Re-enter password:') !!}
    {{--name, default, attribute array--}}
    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'example@dbu.dk']) !!}
</div>

<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => '+45 12 34 56 78']) !!}
</div>

<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    {{--name, default, attribute array--}}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitBtnText, ['class' => 'btn btn-success form-control']) !!}
</div>
