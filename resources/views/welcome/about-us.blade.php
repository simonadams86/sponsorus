@extends('app')
@section('heading')
About Us
@stop
@section('content')

        <h5>Made by</h5>

        @foreach($aboutUs as $title => $info)
            <ul>
                @foreach($info as $item)
                    <li>{{$item}}</li>
                @endforeach
            </ul>
        @endforeach
@stop