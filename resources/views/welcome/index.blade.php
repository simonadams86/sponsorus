@extends('app')

@section('heading')
    Welcome
@stop

@section('content')
    <div class="row">

        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-heading">
                    Who we are:
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-10">We are a matchmaker for established sports clubs and companies. We create a connection between the
                            two parties to help them with mutual beneficial agreements in regards to sponsorships.<br>
                            Integer eget molestie eros, eu bibendum lectus. Cras volutpat fringilla odio, ac molestie diam tristique eget.
                            Etiam ultricies sapien tortor, commodo fermentum tortor consequat ut.
                            Etiam ex velit, fringilla quis venenatis at, sollicitudin eu urna.
                            Nulla sit amet ante ac elit placerat mollis ut quis erat.
                        </div>
                        <div class="col-sm-2">
                            <div class="img-container"><img src="{{asset('img/logo.png')}}"></div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Our cases:
                </div>
                <div class="panel-body">
                    <div class="panel-body">

                        <div class="panel-heading">
                            Companies
                        </div>
                        <div class="col-sm-6">
                            <div class="img-container">
                                <img src="{{ asset('img/noma.jpg') }}" alt="">
                                <div class="row">
                                    <div class="col-sm-12 case-description">
                                        <h4>Noma</h4>
                                        <small>
                                            For a long time we have thought about supporting the local community by sponsoring
                                            a club.<br>
                                            The platform made finding a projects we believe in fast and easy and not time consuming.
                                            We found two different clubs who we decided to sponsor. Both within weeks of signing up
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="img-container">
                                <img src="{{ asset('img/novo-nordisk.gif') }}" alt="">
                                <div class="row">
                                    <div class="col-sm-12 case-description">
                                        <h4>Novo Nordisk</h4>
                                        <small>
                                            For a long time we have thought about supporting the local community by sponsoring
                                            a club.<br>
                                            The platform made finding a projects we believe in fast and easy and not time consuming.
                                            We found two different clubs who we decided to sponsor. Both within weeks of signing up
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="img-container">
                                <img src="{{ asset('img/radiometer.png') }}" alt="">
                                <div class="row">
                                    <div class="col-sm-12 case-description">
                                        <h4>Radiometer Medical</h4>
                                        <small>
                                            For a long time we have thought about supporting the local community by sponsoring
                                            a club.<br>
                                            The platform made finding a projects we believe in fast and easy and not time consuming.
                                            We found two different clubs who we decided to sponsor. Both within weeks of signing up
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="img-container">
                                <img src="{{ asset('img/jensens-bofhus.jpg') }}" alt="">
                                <div class="row">
                                    <div class="col-sm-12 case-description">
                                        <h4>Jensen's Bøfhus</h4>
                                        <small>
                                            For a long time we have thought about supporting the local community by sponsoring
                                            a club.<br>
                                            The platform made finding a projects we believe in fast and easy and not time consuming.
                                            We found two different clubs who we decided to sponsor. Both within weeks of signing up
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">


                        <div class="panel-heading">
                            Clubs
                        </div>
                        <div class="col-sm-6">
                            <div class="img-container">
                                <img src="{{ asset('img/torgersen-fc.jpg') }}" alt="">
                                <div class="row">
                                    <div class="col-sm-12 case-description">
                                        <h4>Torgersen FC</h4>
                                        <small>
                                            We had been looking for a sponsor for more than four years. After signing up
                                            on
                                            SponsorUs it
                                            only took us two months before we partnered up with a company
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="img-container">
                                <img src="{{ asset('img/bk-avarta.jpg') }}" alt="">
                                <div class="row">
                                    <div class="col-sm-12 case-description">
                                        <h4>BK Avarta</h4>
                                        <small>
                                            We had been looking for a sponsor for more than four years. After signing up
                                            on
                                            SponsorUs it
                                            only took us two months before we partnered up with a company
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="img-container">
                                <img src="{{ asset('img/cheung-city.jpg') }}" alt="">
                                <div class="row">
                                    <div class="col-sm-12 case-description">
                                        <h4>Cheung City</h4>
                                        <small>
                                            We had been looking for a sponsor for more than four years. After signing up
                                            on
                                            SponsorUs it only took us two months before we partnered up with a company
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="img-container">
                                <img src="{{ asset('img/ac-iben.jpg') }}" alt="">
                                <div class="row">
                                    <div class="col-sm-12 case-description">
                                        <h4>AC Iben</h4>
                                        <small>
                                            We had been looking for a sponsor for more than four years. After signing up
                                            on
                                            SponsorUs it only took us two months before we partnered up with a company
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop