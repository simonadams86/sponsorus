@extends('app')

@section('heading')
    Contact Us
@stop
@section('content')
        @foreach($contact as $k => $c)
            <div class="row">
                <div class="col-sm-12">
                    <h5 class="contact-info">{{ucfirst($k)}}: {{$c}}</h5>
                </div>
            </div>
        @endforeach
@stop