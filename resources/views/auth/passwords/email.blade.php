@extends('app')

@section('heading')
    Reset Password
@stop

@section('content')
    <div class="row">
        <div class="col-sm-6 col-centered">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            {!! Form::open(['url' => '/password/email']) !!}
            {{ csrf_field() }}

            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">

                {!! Form::label('email', 'Email:') !!}
                {!! Form::text('email', old('email'), ['class' => 'form-control', 'placeholder' => 'example@dbu.dk']) !!}

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                {!! Form::submit('Send Password Reset Link', ['class' => 'btn btn-primary form-control']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
