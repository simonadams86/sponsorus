@extends('app')

@section('heading')
    Reset Password
@stop

@section('content')
    <div class="row">
        <div class="col-sm-6 col-centered">
            {!! Form::open(['url' => '/password/reset']) !!}
            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::label('email', 'Email:') !!}
                {!! Form::text('email', $email or old('email'), ['class' => 'form-control', 'placeholder' => 'example@dbu.dk', 'required', 'autofocus']) !!}
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">

                {!! Form::label('password', 'Password:') !!}
                {!! Form::password('password', ['class' => 'form-control', 'required']) !!}

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">

                {!! Form::label('password_confirmation', 'Confirm Password:') !!}
                {!! Form::password('password_confirmation', ['class' => 'form-control', 'required']) !!}

                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                {!! Form::submit('Reset Password', ['class' => 'btn btn-success form-control']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
