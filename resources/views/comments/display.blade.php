@extends('app')

@section('heading')
    Comments
@stop

@section('content')
    <?php /*var_dump($data['relations']['comments']);exit()*/?>
    @if($data)
        <div class="panel panel-heading">
            <i class=" glyphicon glyphicon-comment {{$data['post']->user_id == Auth::user()->user_id ? 'author' : 'commenter'}}"></i>
            Topic: {!! $data['post']->title !!}
            <div>{{$data['post']['relations']['user']->name}}</div>
        </div>
        <div class="panel-body">{!! $data['post']->message !!}</div>

        @if(count($data['comments']))
            <div><strong>Comments:</strong></div>
            <ul class="list-group">

                @foreach($data['comments'] as $comment)
                    <li class="list-group-item clearfix">

                        @if($comment['relations']['userCommenter']->user_id != Auth::user()->user_id)
                            <div class="row">
                                <div class="col-sm-11 col-centered">
                                    <i class="glyphicon glyphicon-user commenter pull-right"></i>
                                    <div class="clearfix message-body right">
                                        <div class="header">
                                            <h4 class="list-group-item-heading pull-right">
                                                {!! $comment['relations']['userCommenter']->name !!}
                                            </h4>
                                            <small class="text-muted">
                                                <span class="glyphicon glyphicon-time"></span>
                                                {!! $comment->created_at->diffForHumans() !!}
                                            </small>
                                        </div>
                                        <p class="list-group-item-text">{!!  $comment->comment !!}</p>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="row">
                                <div class="col-sm-11 col-centered">
                                    <i class="glyphicon glyphicon-user author pull-left"></i>
                                    <div class="clearfix message-body left">
                                        <div class="header">
                                            <small class="pull-right text-muted">
                                                <span class="glyphicon glyphicon-time"></span>
                                                {!! $comment->created_at->diffForHumans() !!}
                                            </small>
                                            <h4 class="list-group-item-heading">
                                                {!! Auth::user()->name !!}
                                            </h4>
                                        </div>
                                        <p class="list-group-item-text">{!!  $comment->comment !!}</p>
                                    </div>
                                </div>
                            </div>
                        @endif

                    </li>
                @endforeach
            </ul>

        @else

            <div class="center-text"><strong>
                    Approach this

                    @if(Auth::user()->user_type)
                        Company
                    @else
                        Club
                    @endif
                </strong>
            </div>

        @endif
    @endif

    {!! Form::open(['action' => ['CommentsController@create', $data['post']->post_id, $other_id ] ]) !!}

    {!! Form::label('comment', 'Reply:') !!}
    {!! Form::textarea('comment', null, ['class' => 'form-control']) !!}
    {!! Form::submit('Reply', ['class' => 'btn btn-success form-control']) !!}

    {!! Form::close() !!}
    <hr/>
    <a class="btn btn-toolbar" href="{{action('PostsController@showCommentsByPost', $data['post']->post_id)}}">Back</a>
@endsection
