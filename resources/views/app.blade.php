<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SponsorUs') }}</title>

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <!-- Styles -->

    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" type="text/css">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/datatable.css') }}" rel="stylesheet" type="text/css">
</head>
<body>

@include('partials.header')

{{--TODO div.container>div.row>div.col-sm-12*4 press tab from one space back!!!!!!!!!--}}
<main>
    <div class="container">
        <div class="row">
            @if(Auth::user())
                @if(Auth::user()->user_type)

                    {{--<div class="col-sm-2">
                        <div class="sidebar-nav-fixed affix">
                            <div class="well">
                                <img src="{{asset('img/subside.jpg')}}" style="width: 98px; height: auto">

                            </div>
                        </div>
                    </div>--}}

                @endif
            @endif
            <div class="col-sm-11 col-centered">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        @yield('heading')
                    </div>
                    <div class="panel-body">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@include('partials.footer')
{{--Insert scripts js files etc--}}
<script type="text/javascript" src="{{ asset('js/jquery-1.12.3.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/font-awesome.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/cases.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/datatable.js') }}"></script>
</body>
</html>
