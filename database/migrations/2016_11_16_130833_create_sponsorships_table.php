<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSponsorshipsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sponsorships', function (Blueprint $table) {
            $table->increments('sponsorship_id');
            $table->integer('receiver_id')->unsigned();
            $table->integer('contributor_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->foreign('receiver_id')->references('user_id')->on('users');
            $table->foreign('contributor_id')->references('user_id')->on('users');
            $table->foreign('type_id')->references('type_id')->on('sponsor_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sponsorships');
    }
}
