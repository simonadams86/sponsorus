<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'WelcomeController@index');
Route::get('contact', 'WelcomeController@contact');
Route::get('about', 'WelcomeController@about');
Route::get('dashboard', 'DashboardController@index');

Route::get('posts', 'PostsController@index');
Route::get('posts/create', 'PostsController@write');
Route::post('posts/create', 'PostsController@create');

Route::get('posts/{id}/edit', 'PostsController@edit');
Route::patch('posts/{id}/edit', 'PostsController@update');
/*Important that this is below create, otherwise /create triggers showCommentsByPost*/
Route::get('posts/{id}', 'PostsController@showCommentsByPost');

Route::get('marketplace', 'MarketplaceController@index');
Route::get('marketplace/profile/{user_id}', 'MarketplaceController@profile');
Route::get('marketplace/table-data-users', 'MarketplaceController@tableDataUsers');
Route::get('marketplace/table-data-posts', 'MarketplaceController@tableDataPosts');

Route::get('comments/{post_id}/{user_id}', 'CommentsController@showCommentsByUser');
Route::post('comments/{post_id}/{user_id}', 'CommentsController@create');

Auth::routes();
Route::get('update-info', 'UpdateUserController@show');
Route::post('update-info', 'UpdateUserController@update');