'use strict';
/**
 * @author Batch Themes Ltd.
 */
/*Set a request token in compliance with Laravel. Put a token in the header in rights-settings.*/
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


(function () {
    $(function () {

        if (!element_exists('#datatable-users')) {
            return false;
        }

        $('#datatable-users').DataTable({

            //url for the update function
            'ajax': './marketplace/table-data-users',
            'rowId': 'id',

            /*Rules for filling the columns*/
            columns: [
                {data: 'name'},
                {
                    data: 'posts.length',

                }
            ]
        });

        var table = $('#datatable-users').DataTable();

        $('#datatable-users tbody').on('click', 'tr', function () {
            console.log(table.row(this).data());

            var tableData = table.row(this).data();

            $('.modal-title').html(tableData.name);

            $('#modal-lg-primary').modal();


            var html = createUserForm(tableData);

            $('#user_id').val(tableData.user_id);

            $('.modal-body').html(html);


        });
    });
})();

(function () {
    $(function () {

        if (!element_exists('#datatable-posts')) {
            return false;
        }
        var tableData;

        $('#datatable-posts').DataTable({

            //url for the update function
            'ajax': './marketplace/table-data-posts',
            'rowId': 'id',

            /*Rules for filling the columns*/
            columns: [
                {data: 'title'},
                {data: 'name'},
            ]
        });

        var table = $('#datatable-posts').DataTable();

        $('#datatable-posts tbody').on('click', 'tr', function () {
            console.log(table.row(this).data());

            tableData = table.row(this).data();

            $('.modal-title').html(tableData.title + ' - ' + tableData.name);

            $('#modal-lg-primary').modal();

            var html = createPostForm(tableData);

            $('#post_id').val(tableData.post_id);
            $('#user_id').val(tableData.user_id);

            $('.modal-body').html(html);

        });

        /*Attach submitting of form to the modal-button.*/
        $('#modal-save').on('click', function (e) {
            $('#form-content').submit();

        });

        /*Make ajax call on submit */
        $('#form-content').on('submit', function (e) {

            e.preventDefault();

            var post_id = $('#post_id').val();
            var user_id = $('#user_id').val();

            var url = post_id ? '/comments/' + post_id + '/' + user_id + '/' : '/marketplace/profile/' + user_id;
            //you dont need the .each, because you are selecting by id
            //Redirects
            $('#form-content').find("input").val("");

            window.location.href = url;
            return false;

            /*$.ajax({
             method: 'POST',
             url: './save-rights',
             data: $(this).serialize(),
             success: function (response) {
             table.ajax.reload();
             }
             });*/

        });

    });
})();

function createPostForm(data) {

    return "<div class='panel panel-default'><div class='panel panel-body'>" +
        data.message +
        "</div></div>";
}
function createUserForm(data) {

    return "<div class='panel panel-default'><div class='panel panel-body'>" +
        data.description +
        "</div></div>"
}

function element_exists(id) {
    if ($(id).length === 0) {
        return false;
    }
    return true;
}


