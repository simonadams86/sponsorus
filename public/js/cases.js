"use strict";

$(document).ready(function () {

    var case_description = $('.case-description');


    $('.img-container').hover(
        function () {
            $(this).find(case_description).addClass('case-display');
        },
        function () {
            $(this).find(case_description).removeClass('case-display');

        }
    );
});