<?php

namespace SponsorUs;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Comment extends Model
{
    use Notifiable;

    protected $primaryKey = 'comment_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'comment', 'user_id', 'post_id', 'show_for'
    ];

    /**
     * Add to $dates thereby making it a Carbon instance
     *
     * @var array
     */
    protected $dates = ['read_at'];

    /**
     * Query scope to assign a name to a where clause.
     * Important to use scope[NameToUse]
     * Makes it possible to trigger method by using e.g. Comment::created($date)
     *
     * @param $query
     * @param $date
     */
    public function scopeCreated($query, $date)
    {
        $query->where('created_at', '<=', Carbon::create('Y-m-d', $date));
    }

    /**
     * Mutators and accessors
     * Laravel behind the scenes stuff when setting some kind of property
     * Naming convention = set{$VarName}Attribute
     *
     * @param $date
     */
    public function setReadAtAttribute($date)
    {
        $this->attributes['read_at'] = Carbon::parse($date);
    }

    /**
     * A comment is owned by a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userCommenter()
    {
        return $this->belongsTo('SponsorUs\User', 'user_id');
    }

    public function userReceiver()
    {
        return $this->belongsTo('SponsorUs\User', 'show_for');
    }

    /**
     * A comment is also related to a post
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo('SponsorUs\Post');
    }
}