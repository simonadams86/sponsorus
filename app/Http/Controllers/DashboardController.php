<?php
/**
 * Created by PhpStorm.
 * User: simonadams
 * Date: 17/11/2016
 * Time: 15.27
 */

namespace SponsorUs\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use SponsorUs\Http\Controllers\Auth\AuthController;
use SponsorUs\Post;
use SponsorUs\User;

class DashboardController extends AuthController
{
    protected function index()
    {
        $data['users'] = User::take(5)
            ->where('user_type', '!=', Auth::user()->user_type)
            ->latest('created_at')
            ->get();

        $data['posts'] = Post::take(5)
            ->where('posts.user_id', '!=', Auth::user()->user_id)
            ->join('users' ,'posts.user_id', '=', 'users.user_id')
            ->where('user_type', '!=', Auth::user()->user_type)
            ->latest('created_at')
            ->get(['posts.*', 'users.name','users.description']);

        return view('dashboard', compact('data'));
    }

    protected function profile($user_id)
    {
        $data['user'] = User::findOrFail($user_id);

        return view('marketplace.profile', compact('data'));
    }
}