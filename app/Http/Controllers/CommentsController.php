<?php

namespace SponsorUs\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RedirectsUsers;
use SponsorUs\Comment;
use SponsorUs\Http\Controllers\Auth\AuthController;
use SponsorUs\Post;
use Validator;

class CommentsController extends AuthController
{

    use RedirectsUsers;

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'comment' => 'required|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param Request $data
     * @param $post_id
     * @param $user_id
     * @return Comment
     */
    protected function create(Request $data, $post_id, $user_id)
    {

        $this->validator($data->all())->validate();

        $createArr = $data->all();
        $createArr[ 'user_id' ] = Auth::user()->user_id;
        $createArr[ 'show_for' ] = $user_id;
        $createArr[ 'post_id' ] = $post_id;

        Comment::create($createArr);

        return redirect('comments/'.$post_id.'/'.$user_id);
    }

    public function showCommentsByUser($post_id, $user_id)
    {
        $authId = Auth::user()->user_id;

        $data[ 'comments' ] = Comment::with(['userCommenter', 'userReceiver'])
            ->where('post_id', '=', $post_id)
            ->where(function ($query) use ($authId, $user_id) {
                $query->where('user_id', $user_id)
                    ->where('show_for', $authId)
                    ->orWhere(function ($query) use ($authId, $user_id) {
                        $query->where('user_id', $authId)
                            ->where('show_for', $user_id);
                    });
            })
            ->oldest('created_at')
            ->get();
        $data[ 'post' ] = Post::with(['comments', 'user'])->find($post_id);

        $other_id = $user_id;

        return view('comments.display', compact(['data', 'other_id']));
    }
}
