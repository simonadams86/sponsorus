<?php

namespace SponsorUs\Http\Controllers\Auth;

use SponsorUs\Http\Controllers\Controller;

class AuthController extends Controller
{

    /**
     * AuthController constructor.
     * Extend this controller for stuff only accessible when logged on
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
}