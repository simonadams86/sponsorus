<?php

namespace SponsorUs\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use SponsorUs\Http\Controllers\Auth\AuthController;
use Illuminate\Http\Request;
use Validator;


class UpdateUserController extends AuthController
{

    //TODO put forms inside e.g. user/posts/comments?
    protected function show()
    {
        return view('forms.update');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return mixed
     */
    protected function update(Request $data)
    {

        $this->validator($data->all())->validate();

        foreach($data->all() as $key => $value) {
            //might be redundant to check?! Depends on re-usability of forms
            if (Auth::user()->$key && $value != '') {
                Auth::user()->$key = $value;
            }
        }
        Auth::user()->save();

        return redirect('/dashboard');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'max:255',
            'email' => 'email|max:255',
            'password' => 'min:6|confirmed',
        ]);
    }
}