<?php

namespace SponsorUs\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use SponsorUs\Http\Controllers\Auth\AuthController;
use SponsorUs\Post;
use Validator;

class PostsController extends AuthController
{

    /*
     * TODO Clean up. Look at relations in ::with() example in CommentsController!
     */

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title' => 'required|min:3|max:255',
            'message' => 'required|min:10',
        ]);
    }

    public function index()
    {
        $user = Auth::user();

        $posts = $user
            ->posts()
            ->latest('created_at')
            ->get();

        $comments = $user
            ->comments()
            ->join('posts', 'posts.user_id', '=', 'comments.show_for')
            ->where('comments.user_id', '=', Auth::user()->user_id)
            ->groupBy('comments.post_id')
            ->latest('comments.created_at')
            ->get(['posts.user_id AS other_id', 'comments.*']);

        $data[ 'posts' ] = $posts;
        $data[ 'comments' ] = $comments;

        return view('posts.all', compact('data'));
    }

    protected function showCommentsByPost($post_id)
    {
        $post = Post::findOrFail($post_id);

        //TODO Must be easier? -If the post does not belong to user, go to inbox!
        if($post->user()->get()[0]->user_id != Auth::user()->user_id)
            return redirect('posts');

        $comments = $post
            ->comments()
            ->join('users', 'users.user_id', '=', 'comments.user_id')
            ->join('posts', 'posts.post_id', '=', 'comments.post_id')
            ->where('comments.user_id', '!=', Auth::user()->user_id)
            ->groupBy('comments.user_id')
            ->oldest('comments.created_at')
            ->get(['comments.user_id AS other_id', 'comments.*', 'users.*']);
        $data[ 'comments' ] = $comments;
        $data[ 'post' ] = $post;

        return view('posts.display', compact('data'));
    }

    protected function write()
    {
        return view('posts.write');
    }

    protected function edit($post_id)
    {
        $data['post'] = Post::findOrFail($post_id);

        return view('posts.edit', compact('data'));
    }

    protected function update(Request $data, $post_id)
    {
        $post = Post::findOrFail($post_id);

        $post->update($data->all());

        return redirect('posts/'.$post->post_id);
    }

    /**
     * @param Request $data
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function create(Request $data)
    {
        $this->validator($data->all())->validate();

        $post = new Post($data->all());

        Auth::user()->posts()->save($post);

        return redirect('posts/'.$post->post_id);
    }
}