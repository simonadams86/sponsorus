<?php

namespace SponsorUs\Http\Controllers;

class WelcomeController extends Controller
{
    public function index()
    {
        return view('welcome.index');
    }

    public function contact()
    {
        $contact = ['address' => 'BitLab','phone' => '123456', 'email' => 'cs@sponsorus.com'];

        return view('welcome.contact', compact('contact'));
    }

    public function about()
    {
        $aboutUs = ['Students' => ['Simon Adams', 'Calvin Cheung', 'Peter Torgersen', 'Kasper Vestergaard']];

        return view('welcome.about-us', compact('aboutUs'));
    }

}