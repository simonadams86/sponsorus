<?php
/**
 * Created by PhpStorm.
 * User: simonadams
 * Date: 23/11/2016
 * Time: 22.23
 */

namespace SponsorUs\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use SponsorUs\Http\Controllers\Auth\AuthController;
use SponsorUs\Post;
use SponsorUs\User;
use Yajra\Datatables\Facades\Datatables;


class MarketplaceController extends AuthController
{
    protected function index()
    {

        $data['users']['headers'] = ['name' => 'Name', 'noOfPosts' => 'Number of posts'];

        $nameHeader = Auth::user()->user_type ? 'Company' : 'Club';
        $sectionOneHeader = Auth::user()->user_type ? 'Companies' : 'Clubs';

        $data['posts']['headers'] = ['title' => 'Title', 'name' => $nameHeader];
        $data['sectionOneH'] = $sectionOneHeader;

        return view('marketplace.all', compact('data'));
    }

    protected function profile($user_id)
    {
        $data['user'] = User::findOrFail($user_id);


        $data['posts'] = $data['user']->posts()->get();

        return view('marketplace.profile', compact('data'));
    }

    protected function tableDataUsers()
    {
        $data['users'] = User::with(['posts'])->where('user_type', '!=', Auth::user()->user_type)
            ->join('posts', 'posts.user_id', '=', 'users.user_id')
            ->latest('created_at')
            ->groupBy('users.user_id')
            ->get(['users.name', 'users.description', 'posts.*']);


        return Datatables::collection($data['users'])->make(true);
    }

    protected function tableDataPosts()
    {
        $data['posts'] = Post::where('posts.user_id', '!=', Auth::user()->user_id)
            ->join('users' ,'posts.user_id', '=', 'users.user_id')
            ->where('user_type', '!=', Auth::user()->user_type)
            ->latest('created_at')
            ->get(['posts.*', 'users.name','users.description']);

        return Datatables::collection($data['posts'])->make(true);

    }

}