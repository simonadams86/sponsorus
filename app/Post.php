<?php

namespace SponsorUs;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Post extends Model
{
    use Notifiable;


    protected $primaryKey = 'post_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message', 'user_id', 'title'
    ];

    //Scope
    public function scopeCreated($query, $date)
    {
    $query->where('created_at', '<=', Carbon::create('Y-m-d', $date));
    }

    /**
     * A message is owned by a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('SponsorUs\User');
    }

    public function comments()
    {
        return $this->hasMany('SponsorUs\Comment');
    }

}