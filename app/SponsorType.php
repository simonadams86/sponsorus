<?php

namespace SponsorUs;

use Illuminate\Database\Eloquent\Model;

class SponsorType extends Model
{
    protected $fillable = [
        'name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sponsorship()
    {
        return $this->hasMany('SponsorUs\Sponsorship');
    }
}
