<?php

namespace SponsorUs;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $primaryKey = 'subscription_id';

    protected $fillable = [
      'name', 'description', 'length'
    ];

    /**
     * A subscription type has many users
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user()
    {
        return $this->hasMany('SponsorUs\User');
    }
}
