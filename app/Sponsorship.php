<?php

namespace SponsorUs;

use Illuminate\Database\Eloquent\Model;

class Sponsorship extends Model
{
    protected $primaryKey = 'sponsorship_id';

    protected $fillable = [
        'receiver_id', 'contributor_id', 'type', 'open'
    ];


    public function receiver()
    {
        return $this->belongsTo('SponsorUs\User', 'receiver_id');
    }

    public function contributor()
    {
        return $this->belongsTo('SponsorUs\User', 'contributor_id');
    }

    public function sponsorType()
    {
        return $this->belongsTo('SponsorUs\SponsorType');
    }
}
